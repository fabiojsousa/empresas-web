import React, { useState, useEffect } from 'react';
import { TextInput, Preloader } from 'react-materialize';

import history from '~/services/history';
import api from '~/services/api';

import { Wrapper, Loading, Container } from './styles';

import logo from '~/assets/logo-home.png';
import mailIcon from './assets/ic-email.svg';
import padlockIcon from './assets/ic-cadeado.svg';
import eyeIcon from './assets/ic-eye.svg';
import outlinedEyeIcon from './assets/ic-outlined-eye.svg';

export default function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [loginError, setLoginError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  function handleEmailChange(e) {
    setEmail(e.target.value);
  }

  function handlePasswordChange(e) {
    setPassword(e.target.value);
  }

  async function handleSubmit(e) {
    setLoading(true);
    setLoginError(false);
    e.preventDefault();

    try {
      const response = await api.post('users/auth/sign_in', {
        email,
        password,
      });
      setLoading(false);

      const { headers } = response;

      const userData = {
        'access-token': headers['access-token'],
        client: headers['client'],
        uid: headers['uid'],
      };

      localStorage.setItem('empresas-web', JSON.stringify(userData));

      history.push('/home');
    } catch (err) {
      setLoading(false);
      setLoginError(true);
    }
  }

  useEffect(() => {
    setLoginError(false);
  }, [email, password]);

  return (
    <Wrapper>
      {loading && (
        <Loading>
          <Preloader />
        </Loading>
      )}
      <Container>
        <img src={logo} alt="Ioasys logo" />
        <h1>
          BEM-VINDO AO
          <br />
          EMPRESAS
        </h1>

        <p>
          Lorem ipsum dolor sit amet, contetur
          <br />
          adipiscing elit. Nunc accumsan.
        </p>

        <form onSubmit={handleSubmit}>
          <div className="input-email">
            <TextInput
              name="email"
              type="text"
              placeholder="E-mail"
              onChange={handleEmailChange}
              required
            />
            <img id="mail-icon" src={mailIcon} alt="Ícone de email" />
            {loginError && (
              <i className="material-icons" id="error-icon">
                error
              </i>
            )}
          </div>
          <div className="input-password">
            <TextInput
              name="senha"
              type={showPassword ? 'text' : 'password'}
              placeholder="Senha"
              onChange={handlePasswordChange}
              required
            />
            <img src={padlockIcon} id="padlock-icon" alt="Ícone de cadeado" />
            {!loginError ? (
              password !== '' && (
                <img
                  id="eye-icon"
                  onClick={() => setShowPassword(!showPassword)}
                  alt="Ícone de olho"
                  src={showPassword ? eyeIcon : outlinedEyeIcon}
                />
              )
            ) : (
              <i className="material-icons" id="error-icon">
                error
              </i>
            )}
          </div>
          {loginError && (
            <span>Credenciais informadas são inválidas, tente novamente.</span>
          )}

          <button type="submit">ENTRAR</button>
        </form>
      </Container>
    </Wrapper>
  );
}
