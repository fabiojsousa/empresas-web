import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
  background-color: #ebe9d7;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Loading = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  background-color: rgba(255, 255, 255, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;

  .circle-clipper .circle {
    border: solid 5px #57bbbc;
  }
`;

export const Container = styled.div`
  width: 100%;
  max-width: 315px;
  text-align: center;
  position: relative;

  img {
    width: 230px;
  }

  h1 {
    margin-top: 33.5px;
    font-size: 20px;
    font-weight: bold;
    color: #383743;
  }

  p {
    margin-top: 10px;
    color: #383743;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 23px;
    align-items: center;

    .input-email,
    .input-password {
      position: relative;
      width: 100%;

      input {
        width: 250px;
        padding: 0 25px;
        color: #494758;
        height: 30px;

        ::placeholder {
          color: #383743;
        }
      }
    }

    .input-password #padlock-icon {
      position: absolute;
      left: 10px;
      top: 20px;
    }

    .input-email #mail-icon {
      position: absolute;
      left: 8px;
      top: 18px;
    }

    .input-password #eye-icon {
      position: absolute;
      right: 10px;
      top: 20px;
    }

    #error-icon {
      position: absolute;
      right: 0;
      top: 20px;
    }

    span {
      font-size: 12px;
      color: #ff0f44;
    }

    #mail-icon {
      width: 25px;
      color: #ff0f44;
    }

    #error-icon {
      color: #ff0f44;
    }

    #eye-icon {
      cursor: default;
      width: 20px;
    }

    #padlock-icon {
      width: 20px;
    }

    button {
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 3.5px;
      width: 300px;
      color: #ffffff;
      font-weight: bold;
      background-color: #57bbbc;
      border: 0;
      height: 44px;
      font-size: 16px;
      margin-top: 23px;

      :hover {
        background-color: #748383;
      }
    }
  }
`;
