import styled from 'styled-components';

export const Wrapper = styled.div`
  min-height: 100%;
  height: auto;
  background-color: #ebe9d7;
`;

export const HeaderContainer = styled.div`
  background-image: linear-gradient(173deg, #ee4c77 25%, #D8466D 40%);
  padding: 0 30px;
  height: 75.5px;
`;

export const HeaderContent = styled.div`
  max-width: 512px;
  margin: 0 auto;
  height: 100%;
  display: flex;
  justify-content: ${props => (props.searching ? 'center' : 'space-between')};
  align-items: center;

  .logo {
    margin: 0 auto;

    img {
      width: 150px;
    }
  }

  .search {
    img {
      width: 35px;
      cursor: pointer;
    }
  }

  .search-actived {
    position: relative;
    width: 100%;
    height: 100%;
    display: flex;

    form {
      display: flex;
      width: 100%;
      img,
      i {
        position: absolute;
      }

      img {
        cursor: default;
        width: 35px;
        left: 0;
        top: 28px;
      }

      i {
        color: #ffffff;
        right: 0;
        top: 33px;
        cursor: pointer;

        :hover {
          color: #57bbbc;
        }
      }

      #search-input {
        margin-top: 20px;
        padding-left: 40px;
        padding-right: 40px;
        color: #ffffff;
        border-bottom-color: #ffffff;

        ::placeholder {
          color: #991237;
        }
      }
    }
  }

  .enterprise-header {
    display: flex;
    width: 100%;
    justify-content: left;
    margin-top: 20px;

    span {
      display: flex;
      justify-content: center;
      align-items: center;
      color: #ffffff;
      font-size: 17px;

      i {
        cursor: pointer;
        margin-right: 20px;
        color: #ffffff;

        :hover {
          color: #57bbbc;
        }
      }
    }
  }
`;

export const MainContainer = styled.div`
  max-width: 512px;
  height: ${props => (props.id === 'items' ? 'auto' : '526px')};
  margin: 15px auto;
  display: flex;
  align-items: ${props => (props.id === 'items' ? 'baseline' : 'center')};
`;

export const MainContent = styled.div`
  width: 100%;
  text-align: center;

  p {
    color: #383743;
    font-size: 16px;
  }

  .circle-clipper .circle {
    border: solid 5px #57bbbc;
  }

  .no-enterprise {
    color: #b5b4b4;
  }

  .itemsContainer {
    display: flex;
    flex-direction: column;

    .wrapper-container {
      display: flex;
      width: 100%;
      height: 100px;
      background-color: #ffffff;
      padding: 0 15px;
      margin-bottom: 10px;
      border-radius: 3px;
      cursor: pointer;

      :hover {
        border: 1px solid #57bbbc;
      }

      .main-container {
        display: flex;
        width: 100%;
        align-items: center;
        padding: 5px 0px;

        .enterprise-logo {
          display: flex;
          align-items: center;
          background-color: #7dbf75;
          color: #ffffff;
          width: 145px;
          height: 80px;
          justify-content: center;

          span {
            font-size: 30px;
            font-weight: bold;
          }
        }

        .enterprise-data {
          display: flex;
          flex-direction: column;
          margin-left: 20px;
          height: 80px;
          text-align: left;
          justify-content: center;

          h3 {
            margin: 0;
            padding: 0;
            color: #1a0e49;
            font-size: 15px;
            font-weight: bold;
          }
          p {
            color: #8d8c8c;
            font-size: 15px;
          }
          small {
            color: #8d8c8c;
          }
        }
      }
    }
  }
`;
