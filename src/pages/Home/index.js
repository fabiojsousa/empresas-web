import React, { useState } from 'react';
import { Preloader } from 'react-materialize';

import api from '~/services/api';
import {
  Wrapper,
  HeaderContainer,
  HeaderContent,
  MainContainer,
  MainContent,
} from './styles';
import logo from '~/assets/logo-nav.png';
import searchIcon from './assets/ic-search-copy.svg';

import EnterpriseDetails from '~/pages/EnterpriseDetails';

export default function Home() {
  const [searching, setSearching] = useState(false);
  const [loading, setLoading] = useState();
  const [searchText, setSearchText] = useState('');
  const [serverResponse, setServerResponse] = useState();
  const [searchData, setSearchData] = useState();
  const [showEnterprise, setShowEnterprise] = useState();
  const [enterprise, setEnterprise] = useState({});

  function handleSearchTextChange(e) {
    setSearchText(e.target.value);
  }

  function ativateSearch() {
    setSearching(true);
    setServerResponse(false);
    setSearchText('');
  }

  function cancelSearch() {
    setSearching(false);
    setLoading(false);
  }

  function shortName(name) {
    const regex = /[^a-zà-ú]/gi;

    const onlyLettersName = name.replace(regex, '');

    const totalLetter = onlyLettersName.length;

    const shortedName = onlyLettersName[0] + onlyLettersName[totalLetter - 1];

    return shortedName.toUpperCase();
  }

  function showEnterpriseDetails(enterprise) {
    const itemsContainer = document.querySelector('.itemsContainer');

    itemsContainer.style.display = 'none';
    enterprise.shortedName = shortName(enterprise.enterprise_name);

    setEnterprise(enterprise);
    setShowEnterprise(true);
  }

  function backSearching() {
    const itemsContainer = document.querySelector('.itemsContainer');
    itemsContainer.style.display = 'flex';
    setShowEnterprise(false);
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    setServerResponse(false);

    const userData = JSON.parse(localStorage.getItem('empresas-web'));

    try {
      const { data } = await api.get('enterprises', {
        headers: {
          'access-token': userData['access-token'],
          client: userData.client,
          uid: userData.uid,
        },
        params: {
          name: searchText,
        },
      });
      console.log('Request finished!');

      setLoading(false);
      setSearchData(data.enterprises);
      setServerResponse(true);
    } catch (e) {
      setLoading(false);
      setServerResponse(false);
      console.log('Something went wrong.', e);
    }
  }

  return (
    <Wrapper>
      <HeaderContainer>
        <HeaderContent searching={searching}>
          {!searching && !showEnterprise ? (
            <>
              <div className="logo">
                <img src={logo} alt="Ioasys logo" />
              </div>
              <div className="search">
                <img
                  src={searchIcon}
                  onClick={ativateSearch}
                  alt="Ícone para busca"
                />
              </div>
            </>
          ) : (
            !showEnterprise && (
              <div className="search-actived">
                <form onSubmit={handleSubmit}>
                  <input
                    type="text"
                    placeholder="Pesquisar"
                    id="search-input"
                    onChange={handleSearchTextChange}
                    defaultValue={searchText}
                    autoFocus
                  />
                  <img
                    src={searchIcon}
                    alt="Ícone para busca"
                    id="search-icon"
                  />
                  <i
                    className="material-icons"
                    id="clear"
                    onClick={cancelSearch}
                  >
                    close
                  </i>
                </form>
              </div>
            )
          )}
          {showEnterprise && (
            <div className="enterprise-header">
              <span>
                <i className="material-icons" onClick={backSearching}>
                  arrow_back
                </i>
                {enterprise.enterprise_name.toUpperCase()}
              </span>
            </div>
          )}
        </HeaderContent>
      </HeaderContainer>
      <MainContainer
        id={
          serverResponse && searchData.length > 0 && searching
            ? 'items'
            : 'noItems'
        }
      >
        <MainContent>
          {!searching && (
            <p className="instruction">Clique na busca para iniciar.</p>
          )}
          {searching && loading && <Preloader />}
          {serverResponse && searchData.length > 0 && searching ? (
            <div className="itemsContainer">
              {searchData.map(d => (
                <div
                  key={d.id}
                  className="wrapper-container"
                  onClick={() => showEnterpriseDetails(d)}
                >
                  <div className="main-container">
                    <div className="enterprise-logo">
                      <span>{shortName(d.enterprise_name)}</span>
                    </div>
                    <div className="enterprise-data">
                      <h3>{d.enterprise_name}</h3>
                      <p>{d.enterprise_type.enterprise_type_name}</p>
                      <small>{d.country}</small>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ) : (
            serverResponse &&
            searching && (
              <p className="no-enterprise">
                Nenhuma empresa foi encontrada para a busca realizada.
              </p>
            )
          )}

          {showEnterprise && <EnterpriseDetails enterprise={enterprise} />}
        </MainContent>
      </MainContainer>
    </Wrapper>
  );
}
