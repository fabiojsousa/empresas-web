import styled from 'styled-components';

export const Container = styled.div`
  padding: 0 10px;
  display: flex;
  margin: 0 auto;

  .main-container {
    display: flex;
    flex-direction: column;
    background-color: #ffffff;
    max-width: 400px;
    min-height: 480px;
    border-radius: 3px;
    align-items: center;
    padding: 20px 25px;

    .logo-container {
      width: 100%;
      display: flex;
      align-items: center;
      background-color: #7dbf75;
      color: #ffffff;
      min-height: 147px;
      justify-content: center;

      span {
        font-size: 40px;
        font-weight: bold;
      }
    }

    .details-container {
      display: flex;
      max-width: 388px;
      margin-top: 20px;
      text-align: left;

      p {
        color: #8d8c8c;
        size: 17px;
      }
    }
  }
`;
