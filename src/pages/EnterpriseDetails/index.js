import React from 'react';
import PropTypes from 'prop-types';

import { Container } from './styles';

export default function EnterpriseDetails({ enterprise }) {
  return (
    <Container>
      <div className="main-container">
        <div className="logo-container">
          <span>{enterprise.shortedName}</span>
        </div>
        <div className="details-container">
          <p>{enterprise.description}</p>
        </div>
      </div>
    </Container>
  );
}

EnterpriseDetails.propTypes = {
  enterprise: PropTypes.object,
};
